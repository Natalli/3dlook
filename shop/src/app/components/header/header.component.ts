import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {Product} from "../../model/product.model";

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {
    products: Product[];
    count: number;
    show: boolean;
    @Output() showMenu = new EventEmitter<{ show: boolean }>();

    constructor() {
    }

    ngOnInit() {
        this.products = JSON.parse(localStorage.getItem('shop')) ? JSON.parse(localStorage.getItem('shop')) : [];
        this.count = this.products.length;
    }

    showSide() {
        this.show = !this.show;
        this.showMenu.emit({show: this.show})
    }

}
