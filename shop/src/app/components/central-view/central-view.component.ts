import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-central-view',
    templateUrl: './central-view.component.html',
    styleUrls: ['./central-view.component.less']
})
export class CentralViewComponent implements OnInit {
    show: boolean;
    constructor() {
    }

    ngOnInit() {
    }

    showSide(data: boolean) {
        this.show = data
    }
}
