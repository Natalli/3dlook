import {Component, OnInit, Input} from '@angular/core';
import {Product} from "../../model/product.model";
import {ProductsService} from "../../service/products.service";

@Component({
    selector: 'app-basket',
    templateUrl: './basket.component.html',
    styleUrls: ['./basket.component.less']
})
export class BasketComponent implements OnInit {
    products: Product[];
    total: number;
    @Input('showMenu') show: boolean;

    constructor(private productService: ProductsService) {
    }

    ngOnInit() {
        this.products = JSON.parse(localStorage.getItem('shop')) ? JSON.parse(localStorage.getItem('shop')) : [];
        this.total = 0;
        this.products.forEach((data) => {
            this.total = this.total + data.price * data.count;
        })
    }

    addCard() {
        this.productService.addCard(this.products)
            .subscribe((data) => {
                console.log(data);
                if (data) {
                    alert('Спасибо за покупку!')
                }
            })
    }

    incr(id) {
        const ind = this.products.filter(item=> {
            return item.id === id
        });
        ind[0].count++;
    }

    decr(id) {
        const ind = this.products.filter(item=> {
            return item.id === id
        });
        ind[0].count--;
    }

    totalProduct() {

        this.total = 0;
        this.products.forEach((data) => {
            this.total = this.total + data.price * data.count;
        });
        return this.total;
    }


}
