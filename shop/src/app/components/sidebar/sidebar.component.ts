import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {Category} from "../../model/category.model";
import {ProductsService} from "../../service/products.service";

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.less']
})
export class SidebarComponent implements OnInit {
    @Output() Filtered = new EventEmitter<{ category: string }>();
    @Output() Sort = new EventEmitter<{ filterId: number }>();
    filters = [
        {name: 'От дорогих к дешевым', id: 1},
        {name: 'От дешевых к дорогим', id: 2},
        {name: 'Популярные', id: 3},
        {name: 'Новые', id: 4},
    ];
    categories: Category[];
    filterId: number;
    setClass: Array<boolean> = [];
    constructor(private productService: ProductsService) {
        this.setClass[0] = true;
    }

    ngOnInit() {
        this.productService.getCategory()
            .subscribe((data: Category[]) => {
                console.log(data);
                this.categories = data;
            })
    }

    setCategory(name) {
        this.Filtered.emit({
            category: name
        });
        return 'active'
    }

    selectFilter(event) {
        this.filterId = event.id;
        this.Sort.emit({
            filterId: this.filterId
        });
    }

    selectElems(index) {
        for (let i = 0; i <= this.categories.length; i++) {
            if (i === index) {
                this.setClass[index] = !this.setClass[index];
            } else {
                this.setClass[i] = false;
            }
        }
    }

}
