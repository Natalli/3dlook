export class Product {
    constructor(
        public category: number,
        public name: string,
        public price: number,
        public total: number,
        public imgs: [],
        public count: number,
        public id?: number) {
    }

}
