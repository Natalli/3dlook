import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from "@angular/common/http";
import {AppRoutingModule} from './app-routing.module';
import {NgSelectModule} from '@ng-select/ng-select';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {HeaderComponent} from './components/header/header.component';
import {CentralViewComponent} from './components/central-view/central-view.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {CatalogComponent} from './pages/catalog/catalog.component';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import {ProductsService} from "./service/products.service";
import {ProductComponent} from './pages/product/product.component';
import {BasketComponent} from './components/basket/basket.component';
import {CategoryPipe} from "./pipe/category";
import { ProductsComponent } from './pages/products/products.component';
import {SortPipe} from "./pipe/sort";

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        CentralViewComponent,
        SidebarComponent,
        CatalogComponent,
        ProductComponent,
        BasketComponent,
        CategoryPipe,
        SortPipe,
        ProductsComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        SlickCarouselModule,
        HttpClientModule,
        NgSelectModule,
        FormsModule
    ],
    providers: [ProductsService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
