import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CentralViewComponent} from "./components/central-view/central-view.component";
import {CatalogComponent} from "./pages/catalog/catalog.component";
import {ProductComponent} from "./pages/product/product.component";


const routes: Routes = [
    {
        path: '', component: CentralViewComponent,
        children: [
            {path: '', component: CatalogComponent}
        ]
    },
    {path: 'product/:id', component: ProductComponent},
    {path: '', redirectTo: '/', pathMatch: 'full'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes,
        {
            useHash: true,
            scrollPositionRestoration: 'enabled'
        })],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
