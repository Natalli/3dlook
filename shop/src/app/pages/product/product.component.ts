import {Component, OnInit} from '@angular/core';
import {Product} from "../../model/product.model";
import {ProductsService} from "../../service/products.service";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.less']
})
export class ProductComponent implements OnInit {
    product: Product;
    products: Product[];
    params: any;
    size = [
        {name: '40'},
        {name: '42'},
        {name: '44'}
    ];
    show: boolean;
    isLoaded: boolean = false;
    cartList: Product[];

    constructor(private productService: ProductsService,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.productService.getProducts()
            .subscribe((data: Product[]) => {
                this.products = data;
                this.params = this.route.params
                    .subscribe((param) => {
                        const id = +param['id'];
                        this.product = this.products.find(item => {
                            return item.id === id
                        });
                        this.isLoaded = true;
                    })
            })
    }

    addShopList(product: Product) {
        this.productService.addShopList(product);
        this.cartList.push(product);
    }

    showSide(data: boolean) {
        this.show = data
    }
}
