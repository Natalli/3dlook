import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-catalog',
    templateUrl: './catalog.component.html',
    styleUrls: ['./catalog.component.less']
})
export class CatalogComponent implements OnInit {

    filterData: object;
    sortData: object;
    constructor() {
    }

    ngOnInit() {

    }

    filtered(data: {category: string, }) {
        console.log('data cent', data);
        this.filterData = data;
        console.log(this.filterData);
    }

    sorted(data: {filterId: number}) {
        this.sortData = data;
    }
}
