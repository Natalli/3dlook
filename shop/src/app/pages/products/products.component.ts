import {Component, Input, OnInit} from '@angular/core';

import {Product} from "../../model/product.model";
import {ProductsService} from "../../service/products.service";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.less']
})
export class ProductsComponent implements OnInit {
  @Input('filterData') filter: { category: string };
  @Input('sortData') sort: { filterId: number };
  products: Product[];
  slideConfig: object;
  slideNavConfig: object;
  constructor(private productsService: ProductsService) {
  }

  ngOnInit() {
    this.productsService.getProducts()
        .subscribe((products: Product[]) => {
          this.products = products;
          this.products.forEach((pr, i) => {
            this.slideConfig = {
              "slidesToShow": 1,
              "slidesToScroll": 1,
              "arrows": false,
              "dots": false,
              "infinite": false,
              "asNavFor": ".product-slide-nav-" + i,
            };
            this.slideNavConfig = {
              "slidesToShow": 4,
              "slidesToScroll": 1,
              "arrows": true,
              "dots": false,
              "infinite": false,
              "asNavFor": ".product-slide-" + i,
              "focusOnSelect": true
            };
          });
        })
  }
}
