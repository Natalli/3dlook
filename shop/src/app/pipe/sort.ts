import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'sort'
})
export class SortPipe implements PipeTransform {
    transform(data, value): any {
        console.log(data);
        console.log(value);
        if (value === undefined) {
            return data;
        } else if (value){
            if(value.filterId === 1) {
                return data.sort((a, b) => {
                    return b.price - a.price
                });

            } else if(value.filterId === 2) {
                return data.sort((a, b) => {
                    return a.price - b.price
                });
            } else if (value.filterId === 3 || value.filterId === 4) {
                return data
            }
        }


        // if (value) {
        //     if(value.category === 'All') {
        //         return data
        //     }
        //     return data.filter(item => {
        //         return item.category === value.category;
        //     });
        // } else {
        //     return data;
        // }
    }
}
