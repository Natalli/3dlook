import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'category'
})
export class CategoryPipe implements PipeTransform {
    transform(data, value): any {
        if (value) {
            if(value.category === 'All') {
                return data
            }
            return data.filter(item => {
                return item.category === value.category;
            });
        } else {
            return data;
        }
    }
}
