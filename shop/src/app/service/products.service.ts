import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Product} from "../model/product.model";

@Injectable()
export class ProductsService {
    url = 'http://localhost:3000';

    constructor(
        private http: HttpClient) {
    }

    getCategory() {
        return this.http.get(this.url + '/category')
    }

    getProducts() {
        return this.http.get(this.url + '/products');
    }

    addShopList(product: Product) {
        const products: Product[] = JSON.parse(localStorage.getItem('shop')) ? JSON.parse(localStorage.getItem('shop')) : [];
        products.push(product);
        window.localStorage.setItem('shop', JSON.stringify(products));
    }

    addCard(products: Product[]) {
        return this.http.post(this.url + '/card', products)
    }
}
